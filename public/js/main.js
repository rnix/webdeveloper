(function($){
    
    $(function(){
        
        var amount = 0;
        var hiredProgrammers = {};
        var hiredBuffs = [];
        var startTime = new Date;
        
        var amountPerClick = 4;
        var updateInterval = 5000;
        var checkPointAmount = 1000000;
        var checkPointWasShown = false;
        var minEstimateProfit = 0;
        var maxEstimateProfit = 0;
        var mainLoopIntervalId;
        
        var programmers = [
            {"pid": 0, "cost": 100, "title": "Быдлокодер", "effect": 4, "failChance": 90},
            {"pid": 1, "cost": 500, "title": "Джуниор", "effect": 5, "failChance": 60},
            {"pid": 2, "cost": 2000, "title": "Мидл", "effect": 10, "failChance": 20},
            {"pid": 3, "cost": 6000, "title": "Сеньор", "effect": 25, "failChance": 10},
            {"pid": 4, "cost": 30000, "title": "Звезда", "effect": 40, "failChance": 0},
        ];
        
        var buffs = [
            {"bid": "cookies", "cost": 200, "title": "Бесплатные печеньки", "effects": {"amountPerTime": -9, "amountPerPid2": 1}},
            {"bid": "table", "cost": 350, "title": "Теннисный стол", "effects": {"none": 0}},
            {"bid": "assist", "cost": 500, "title": "Личный помощник", "effects": {"amountPerClick": 1}},
            {"bid": "designerMarketolog", "cost": 820, "title": "Дизайнер-маркетолог", "effects": {"amountPerTime": 1, "amountPerPid0": 1}},
            {"bid": "frontStudent", "cost": 850, "title": "Верстальщик-студент", "effects": {"amountPerPid1": 1}},
            {"bid": "designer", "cost": 1880, "title": "Дизайнер", "effects": {"amountPerTime": 4, "amountPerPid0": -1, "amountPerPid2": 1}},
            {"bid": "front", "cost": 1900, "title": "Верстальщик", "effects": {"amountPerPid1": 1, "amountPerPid2": 1}},
            {"bid": "frontExp", "cost": 3000, "title": "Опытный верстальщик", "effects": {"amountPerPid0": -2, "amountPerPid3": 2, "amountPerPid4": 3}},
            {"bid": "xbox", "cost": 3500, "title": "Приставка (+телевизор)", "effects": {"amountPerTime": -10, "pricePerPid0": 1, "pricePerPid1": 1, "pricePerPid2": 1}},
            {"bid": "pm", "cost": 4000, "title": "ПМ", "effects": {"amountPerPid0": -3, "amountPerPid2": 2, "amountPerPid3": 3, "amountPerPid4": 6}},
            {"bid": "designerExp", "cost": 5000, "title": "Опытный дизайнер", "effects": {"amountPerTime": 7, "amountPerPid0": -1, "amountPerPid2": 1, "amountPerPid3": 2, "amountPerPid4": 6}},
            {"bid": "seo", "cost": 5000, "title": "Сеошник", "effects": {"amountPerTime": -10, "amountPerPid1": 1, "amountPerPid2": 2, "amountPerPid3": 3, "amountPerPid4": 3}},
            {"bid": "teamlead", "cost": 8000, "title": "Тимлид", "effects": {"amountPerPid0": -4, "amountPerPid1": -3, "amountPerPid2": 5, "amountPerPid3": 7, "amountPerPid4": -2}},
            {"bid": "buyos", "cost": 12000000000, "title": "Сделать вклад в Open Source", "effects": {"endOs": 0}},
            {"bid": "buyms", "cost": 260000000000, "title": "Купить Мекрософт", "effects": {"endMs": 0}}
        ];
        
        
        function updateAmount(){
            $(".amount-value").html(amount);
        }
        
        $(".button-container .make-btn").on("click", function(){
            $(this).blur();
            $.each(hiredBuffs, function(i, buff){
                $.each(buff.effects, function(effectName, effectValue){
                    if (effectName == 'amountPerClick'){
                        amount += effectValue;
                    }
                });
            });
            amount += amountPerClick;
            updateAmount();
            checkCheckPoint();
        });
        
        $(".programmers-list").on("click", ".hire-btn", function(e){
            e.preventDefault();
            var pid = $(this).data('pid');
            $.each(programmers, function(i, progr){
               if (pid == progr.pid && amount >= progr.cost){
                   amount -= progr.cost;
                   updateAmount();
                   hireProgrammer(progr);
               }
            });
        });
        
        $(".buffs-list").on("click", ".hire-btn", function(e){
            e.preventDefault();
            var bid = $(this).data('bid');
            $.each(buffs, function(i, buff){
               if (bid == buff.bid && amount >= buff.cost){
                   amount -= buff.cost;
                   updateAmount();
                   hireBuff(buff);
               }
            });
        });
        
        function setAvailableProgrammers(programmers){
            var $container = $(".programmers-list tbody").empty();
            var rowTpl = $("#programmerTpl").html();
            $.each(programmers, function(i, progr){
               progr["successChance"] = 100 - progr["failChance"];
               progr["updateInterval"] = Math.round(updateInterval/1000);
               var row = Mustache.render(rowTpl, progr);
               $container.append(row);
            });
        }
        
        function setAvailableBuffs(buffs){
            var $container = $(".buffs-list tbody").empty();
            var rowTpl = $("#buffTpl").html();
            $.each(buffs, function(i, buff){
                buff.effectsDescCompiled = "<li>" + buff.effectsDesc.join("</li> <li>") + "</li>";
                var row = Mustache.render(rowTpl, buff);
                $container.append(row);
            });
        }
        
        function buildBuffsDescriptions(){
            $.each(buffs, function(i, buff){
                buff.effectsDesc = [];
                $.each(buff.effects, function(effectName, effectValue){
                    var desc = effectName + "=" + effectValue;
                    var compare = '<span class="positive">больше</span>';
                    var sign = '';
                    if (effectValue < 0){
                        compare = '<span class="negative">меньше</span>';
                        effectValue *= -1;
                        sign = '<span class="negative">—</span>';
                    }
                    switch(effectName){
                        case "amountPerTime":
                            desc = sign + "$" + effectValue + " каждые " + Math.round(updateInterval/1000) + "с";
                            break;
                        case "amountPerPid0":
                            desc = "каждый быдлокодер приносит на $" + effectValue + " " + compare;
                            break;
                        case "amountPerPid1":
                            desc = "каждый джуниор приносит на $" + effectValue + " " + compare;
                            break;
                        case "amountPerPid2":
                            desc = "каждый мидл приносит на $" + effectValue + " " + compare;
                            break;
                        case "amountPerPid3":
                            desc = "каждый сеньор приносит на $" + effectValue + " " + compare;
                            break;
                        case "amountPerPid4":
                            desc = "каждый прогер-звезда приносит на $" + effectValue + " " + compare;
                            break;
                        case "none":
                            desc = "ничего не даёт, но вроде как нужен";
                            break;
                        case "amountPerClick":
                            desc = "каждый ваш личный сайт приносит на $" + effectValue + " " + compare;
                            break;
                        case "pricePerPid0":
                            desc = "снижает стоимость быдлокодера на $" + effectValue;
                            break;
                        case "pricePerPid1":
                            desc = "снижает стоимость джуниора на $" + effectValue;
                            break;
                        case "pricePerPid2":
                            desc = "снижает стоимость мидла на $" + effectValue;
                            break;
                        case "endMs":
                            desc = "увеличивает ваши доходы, а может и нет";
                            break;
                        case "endOs":
                            desc = "увеличивает эффективность всех разработчиков";
                            break;
                    }
                    buff.effectsDesc.push(desc);
                })
            });
        }
        
        
        function countHiredBuffsByBid(bid){
            var count = 0;
            $.each(hiredBuffs, function(i, buff){
                if (buff.bid == bid){
                    count++;
                }
            });
            return count;
        }
        
        function updateEstimateProfit(){
            minEstimateProfit = 0;
            maxEstimateProfit = 0;
            
            $.each(hiredBuffs, function(i, buff){
                $.each(buff.effects, function(effectName, effectValue){
                    if (effectName == 'amountPerTime'){
                        minEstimateProfit += effectValue;
                        maxEstimateProfit += effectValue;
                    }
                });
            });
            
            $.each(hiredProgrammers, function(pid, progrInfo){
                if (progrInfo.progr.failChance == 0){
                    minEstimateProfit += progrInfo.progr.effect * progrInfo.q;
                }
                maxEstimateProfit += progrInfo.progr.effect * progrInfo.q;
            });
            
            $(".amount-value").attr('title', '+ $' + minEstimateProfit + ' — $' + maxEstimateProfit);
        }
        
        function hireProgrammer(progr){
            if (typeof hiredProgrammers[progr.pid] == 'undefined'){
                hiredProgrammers[progr.pid] = {'progr': progr, 'q': 1};
                $(".hire-tip").hide();
            } else {
                hiredProgrammers[progr.pid].q += 1;
            }
            
            var rowTpl = $("#hiredProgrammerTpl").html();
            var $existedRow = $('.hired-programmers-list tr[data-pid="' + progr.pid + '"]');
            var renderedRow = Mustache.render(rowTpl, $.extend(progr, {"num": hiredProgrammers[progr.pid].q}));
            if ($existedRow.length){
                $existedRow.replaceWith(renderedRow);
            } else {
                $(".hired-programmers-list tbody").prepend(renderedRow);
            }
            
            updateEstimateProfit();
        }
        
        function hireBuff(buff) {
            hiredBuffs.push(buff);
            var rowTpl = $("#hiredBuffTpl").html();
            var $existedRow = $('.hired-buffs-list tr[data-bid="' + buff.bid + '"]');
            var renderedRow = Mustache.render(rowTpl, $.extend(buff, {"num": countHiredBuffsByBid(buff.bid)}));
            if ($existedRow.length) {
                $existedRow.replaceWith(renderedRow);
            } else {
                $(".hired-buffs-list tbody").prepend(renderedRow);
            }
            applyBuffEffects(buff);
        }
        
        function applyBuffEffects(buff){
            $.each(buff.effects, function(effectName, effectValue){
                switch(effectName){
                    case "amountPerPid0":
                        getProgrammerByPid(0).effect += effectValue;
                        break;
                    case "amountPerPid1":
                        getProgrammerByPid(1).effect += effectValue;
                        break;
                    case "amountPerPid2":
                        getProgrammerByPid(2).effect += effectValue;
                        break;
                    case "amountPerPid3":
                        getProgrammerByPid(3).effect += effectValue;
                        break;
                    case "amountPerPid4":
                        getProgrammerByPid(4).effect += effectValue;
                        break;
                    case "pricePerPid0":
                        getProgrammerByPid(0).cost = Math.max(1, getProgrammerByPid(0).cost - effectValue);
                        break;
                    case "pricePerPid1":
                        getProgrammerByPid(1).cost = Math.max(1, getProgrammerByPid(1).cost - effectValue);
                        break;
                    case "pricePerPid2":
                        getProgrammerByPid(2).cost = Math.max(1, getProgrammerByPid(2).cost - effectValue);
                        break;
                    case "pricePerPid3":
                        getProgrammerByPid(3).cost = Math.max(1, getProgrammerByPid(3).cost - effectValue);
                        break;
                    case "endMs":
                        window.setTimeout(function(){
                            $("body").empty().addClass('end-ms');
                        }, 5000);
                        endGame();
                        break;
                    case "endOs":
                        $(".game-row, .header, footer").hide();
                        $(".end-os-alert").show();
                        endGame();
                        break;
                }
            });
            
            updateAvailableProgrammers();
            updateHiredProgrammers();
            updateEstimateProfit();
        }
        
        function endGame(){
            window.clearInterval(mainLoopIntervalId);
            amount = 0;
            updateAmount();
        }
             
        function getProgrammerByPid(pid){
            for (var i=0; i<programmers.length; i++){
                var progr = programmers[i];
                if (pid == progr.pid){
                    return progr;
                }
            }
        }
        
        function updateHiredProgrammers(){
            var rowTpl = $("#hiredProgrammerTpl").html();
            var $container = $(".hired-programmers-list tbody").empty();
            $.each(hiredProgrammers, function(pid, progrInfo) {
                var $existedRow = $('.hired-programmers-list tr[data-pid="' + pid + '"]');
                var renderedRow = Mustache.render(rowTpl, $.extend(progrInfo.progr, {"num": progrInfo.q}));
                if ($existedRow.length) {
                    $existedRow.replaceWith(renderedRow);
                } else {
                    $container.prepend(renderedRow);
                }
            });
        }
        
        function updateAvailableProgrammers(){
            setAvailableProgrammers(programmers);
        }
        
        function checkCheckPoint(){
            if (!checkPointWasShown && amount >= checkPointAmount){
                var currentTime = new Date();
                var past = Math.round((currentTime - startTime) / 1000);
                var $checkPointAlert = $(".checkpoint-alert");
                $checkPointAlert.html(Mustache.render($checkPointAlert.html(), {'amount': checkPointAmount, 'time': past}));
                $checkPointAlert.show();
                $checkPointAlert.append($("#checkPointTwitTpl").html());
                checkPointWasShown = true;
                window.scrollTo(0, 0);
            }
        }
        
        function getRandomInt(min, max) {
            return min + Math.floor(Math.random() * (max - min + 1));
        }
        
        mainLoopIntervalId = window.setInterval(function(){
            
            $.each(hiredBuffs, function(i, buff){
                $.each(buff.effects, function(effectName, effectValue){
                    if (effectName == 'amountPerTime'){
                        amount += effectValue;
                    }
                });
            });
            
            $.each(hiredProgrammers, function(pid, progrInfo){
                
                for (var i=0; i<progrInfo.q; i++){
                    var random = getRandomInt(0, 100);
                    if (random >= progrInfo.progr.failChance){
                        amount += progrInfo.progr.effect;
                    }
                }
                
            });
            
            updateAmount();
            checkCheckPoint();
        }, updateInterval);
        
        
        setAvailableProgrammers(programmers);
        buildBuffsDescriptions();
        setAvailableBuffs(buffs);
        updateAmount();
    });
    
})(jQuery);